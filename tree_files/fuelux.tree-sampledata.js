var DataSourceTree = function(options) {
	this._data 	= options.data;
	this._delay = options.delay;
}

DataSourceTree.prototype.data = function(options, callback) {
	var self = this;
	var $data = null;

	if(!("name" in options) && !("type" in options)){
		$data = this._data;//the root tree
		callback({ data: $data });
		return;
	}
	else if("type" in options && options.type == "folder") {
		if("additionalParameters" in options && "children" in options.additionalParameters)
			$data = options.additionalParameters.children;
		else $data = {}//no data
	}
	
	if($data != null)//this setTimeout is only for mimicking some random delay
		setTimeout(function(){callback({ data: $data });} , parseInt(Math.random() * 500) + 200);

	//we have used static data here
	//but you can retrieve your data dynamically from a server using ajax call
	//checkout examples/treeview.html and examples/treeview.js for more info
};

var tree_data = {
	'系统设置' : {name: '系统设置', type: 'folder'},
	'权限与角色管理' : {name: '权限与角色管理', type: 'folder'},
	'会员管理' : {name: '会员管理', type: 'folder'}	,
	'信审管理' : {name: '信审管理', type: 'folder'}	,
	'贷款管理' : {name: '贷款管理', type: 'folder'}	,
	'放款异常管理' : {name: '放款异常管理', type: 'folder'}	,
	'催款管理' : {name: '催款管理', type: 'folder'}	,
	'数据统计' : {name: '数据统计', type: 'folder'}	,
	'信息管理' : {name: '信息管理', type: 'folder'}	,
	'优惠券管理' : {name: '优惠券管理', type: 'folder'},
	'广告管理' : {name: '广告管理', type: 'folder'}	,
	'基本信息' : {name: '基本信息', type: 'folder'}	,
	'登录日志' : {name: '登录日志', type: 'folder'}	,
	'系统数据备份' : {name: '系统数据备份', type: 'folder'}	
}
tree_data['系统设置']['additionalParameters'] = {
	'children' : {
		'利率设置' : {name: '利率设置', type: 'folder'},
		'合同模板' : {name: '合同模板', type: 'folder'},
		'短信模板' : {name: '短信模板', type: 'folder'},
		'参数设置' : {name: '参数设置', type: 'folder'}
	}
}
tree_data['权限与角色管理']['additionalParameters'] = {
	'children' : {
		'系统管理员' : {name: '系统管理员', type: 'folder'},
		'角色列表' : {name: '角色列表', type: 'folder'}
	}
}
tree_data['会员管理']['additionalParameters'] = {
	'children' : {
		'会员列表' : {name: '会员列表', type: 'folder'}
	}
}
tree_data['信审管理']['additionalParameters'] = {
	'children' : {
		'基本信息认证' : {name: '基本信息认证', type: 'folder'},
		'身份认证' : {name: '身份认证', type: 'folder'},
		'手机认证' : {name: '手机认证', type: 'folder'},
		'银行卡认证' : {name: '银行卡认证', type: 'folder'},
		'京东认证' : {name: '京东认证', type: 'folder'},
		'淘宝认证' : {name: '淘宝认证', type: 'folder'},
	}
}
tree_data['登录日志']['additionalParameters'] = {
	'children' : {
		'后台管理登录日志' : {name: '后台管理登录日志', type: 'folder'},
		'App登录日志' : {name: 'App登录日志', type: 'folder'}
	}
}
tree_data['贷款管理']['additionalParameters'] = {
	'children' : {
		'机审拒绝' : {name: '机审拒绝', type: 'folder'},
		'待人工审核' : {name: '待人工审核', type: 'folder'},
		'贷款申请未通过' : {name: '贷款申请未通过', type: 'folder'},
		'待放款管理' : {name: '待放款管理', type: 'folder'},
		'放款记录' : {name: '放款记录', type: 'folder'},
		'正常还款' : {name: '正常还款', type: 'folder'},
		'逾期订单' : {name: '逾期订单', type: 'folder'},
		'逾期结清订单' : {name: '逾期结清订单', type: 'folder'}
	}
}
tree_data['催款管理']['additionalParameters'] = {
	'children' : {
		'正常待还款客户' : {name: '正常待还款客户', type: 'folder'},
		'逾期待催款客户' : {name: '逾期待催款客户', type: 'folder'},
		'坏账管理' : {name: '坏账管理', type: 'folder'},
		'黑名单' : {name: '黑名单', type: 'folder'},
		'我的催收列表' : {name: '我的催收列表', type: 'folder'}
	}
}
tree_data['放款异常管理']['additionalParameters'] = {
	'children' : {
		'打款失败' : {name: '打款失败', type: 'folder'},
		'疑似重复订单' : {name: '疑似重复订单', type: 'folder'}
	}
}
tree_data['数据统计']['additionalParameters'] = {
	'children' : {
		'统计报表' : {name: '统计报表', type: 'folder'}
	}
}
tree_data['信息管理']['additionalParameters'] = {
	'children' : {
		'信息管理' : {name: '信息管理', type: 'folder'}
	}
}
tree_data['优惠券管理']['additionalParameters'] = {
	'children' : {
		'优惠券设置' : {name: '优惠券设置', type: 'folder'},
		'优惠券人员管理' : {name: '优惠券人员管理', type: 'folder'}
	}
}
tree_data['广告管理']['additionalParameters'] = {
	'children' : {
		'广告发布与管理' : {name: '广告发布与管理', type: 'folder'}
	}
}
tree_data['基本信息']['additionalParameters'] = {
	'children' : {
		'用户反馈信息' : {name: '用户反馈信息', type: 'folder'},
		'用户注册协议' : {name: '用户注册协议', type: 'folder'},
		'关于我们' : {name: '关于我们', type: 'folder'}
	}
}
tree_data['系统数据备份']['additionalParameters'] = {
	'children' : {
		'数据备份' : {name: '数据备份', type: 'folder'}
	}
}
findAll();
function findAll(){
	$.ajax({
		url: urlcore + "/api/thirdCatalogue/findAll?roleId="+roleId,
		type: "GET",
		async: false,
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		success:function(data){
		if (data.success == true) {
			$.each(data.data, function(i,n) {
				var html = '{"children":{';
				$.each(n.first.second.third, function(m,l) {
					if (m != 0) {
						html+=",";
					}
					if (l.select == 1) {
						myArray.push(l.data);
					}
					html+='"'+l.title+'":{"name":"'+l.title+'","type": "item","data":'+l.data+',"selected":'+l.select+'}';
				});
				html+="}}";
				console.log(html);
				html = jQuery.parseJSON(html);
				tree_data[''+n.first.title+'']['additionalParameters']['children'][''+n.first.second.title+'']['additionalParameters']=html
			});
		} else {
			alert(data.msg);
		}
 
		},
		error:function() {
			alert("error");
		}
	});
}

var treeDataSource = new DataSourceTree({data: tree_data});
