var gmtDatetime = '';
var name = '';
var orderStatus = '';
var phoneNumber = '';
var currentPage = 1;
var totalMoney = 0;
var totalPeople = 0;
var jName = getCookie('Jname');
//我的权限数组
var arrayTitle = new Array; 
loadMyEssay(gmtDatetime, name, phoneNumber, orderStatus);
countPeopleMoney(gmtDatetime, name, phoneNumber,orderStatus);

function loadMyEssay(gmtDatetime, name, phoneNumber, orderStatus) {

	$(document).ready(function() {
		findMyCatalogue();
		init(currentPage);
	});

	function init(pageNo) {
		$("#thislist").html("");
		$.ajax({
			url: urlcore + "/api/loanOrder/allPassMoneyList?gmtDatetime=" + gmtDatetime + "&name=" + name + "&phoneNumber=" + phoneNumber + "&orderStatus=" + orderStatus + "&current=" + pageNo,
			type: "get",
			async: 'false',
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				if(data.success == true) {
					$.each(data.data.list, function(i, n) {

						var id = n.id;
						var status = '';
						if(n.orderStatus == "3") {
							status = "待还款"
						} else if(n.orderStatus == "4") {
							status = "容限期中";
						} else if(n.orderStatus == "5") {
							status = "超出容限期中";
						} else if(n.orderStatus == "6") {
							status = "已还款";
						} else if(n.orderStatus == "8") {
							status = "坏账";
						}
						var thislist =
							'<tr class="footable-even" style="display: table-row;">' +

							'<td class="footable-visible">' + n.id + '</td>' +
							'<td class="footable-visible">' + n.orderNumber + '</td>' +
							'<td class="footable-visible">' + n.user.userName + '</td>' +
							'<td class="footable-visible">' + n.user.phone + '</td>' +
							'<td class="footable-visible">' + n.lianRayNum + '</td>' +
							'<td class="footable-visible">' + n.limitDays + '</td>' +
							'<td class="footable-visible">' + n.borrowMoney + '</td>' +
								'<td class="footable-visible">' + n.realMoney + '</td>' +
							'<td class="footable-visible">' + n.needPayMoney + '</td>' +
							'<td class="footable-visible">' + n.giveTime + '</td>' +
							'<td class="footable-visible">' + n.limitPayTime + '</td>' +
							'<td class="footable-visible">' + status + '</td>' +
							'<td class="footable-visible footable-last-column">'+
								'<a  class="btn btn-primary btn-xs" name="确认还款" onclick="confirmReceive('+n.id+')">确认还款</a>&nbsp;'+
								'<a hidden="hidden" class="" name="查看认证信息" href="tab.html?id='+n.userId+'&userName='+escape(n.user.userName)+'&phone='+n.user.phone+'"  >查看认证信息</a>&nbsp;'+
								'<a hidden="hidden" class="" name="详情" href="all_loan_list_detail.html?id=' + n.id + '" >详情</a></td>'+
							'</tr>';
					
						$('#thislist').append(thislist);

					});
					$.each(arrayTitle, function(i,k) {
						$('a[name="'+k+'"]').attr("hidden",false).attr("class","btn btn-primary btn-xs");
					});
					$("#pager").pager({
						pagenumber: pageNo,
						pagecount: data.data.pages,
						totalcount: data.data.total,
						buttonClickCallback: PageClick
					});

					if(data.code == 'OVERTIME') {
						var thisUrl = window.location.href;

						if(thisUrl.indexOf('login.html') <= -1) {
							top.window.location.href = "login.html";
						}

					} else {
						if(data.msg != '空数据') {
							//alert(data.msg)
						} else {
							$('#thiscount').text(0);
						}
					}
				}

			},
			error: function() {
				alert("error");
			}
		});
	}
	PageClick = function(pageclickednumber) {
		init(pageclickednumber);
	}

}

//确认还款操作
function confirmReceive(orderId){
	if(confirm("确定该订单已还款吗")){
        $.ajax({
            url: urlcore + "/api/loanOrder/receiveMoneyAdmin",
            type: "GET",
			data:{orderId:orderId},
            dataType: 'json',
            async: false,
            contentType: "application/json;charset=utf-8",
            success:function(data){
                if (data.success == true) {
                   alert("确认还款成功");
                   window.location.reload();
                } else {
                    alert(data.msg);
                }

            },
            error:function() {
                alert("error");
            }
        });
	}
}

function EnterSearchList() {
	var code = event.keyCode;
	if(code == 13) {
		searchList();
	}

}

function searchList() {

	var phoneNumber = $('#phoneNumber').val().trim();
	var userName = $('#userName').val().trim();
	var time = $('#applyTime').val().trim();
	loadMyEssay(time, userName, phoneNumber, '');
	countPeopleMoney(time, userName, phoneNumber,'');

}

function checkAuthDetails(id) {

}

function selectOrdersStatus(orderStatus) {
	loadMyEssay('', '', '', orderStatus);
	countPeopleMoney('', '', '', orderStatus);
}

function countPeopleMoney(gmtDatetime, name, phoneNumber, orderStatus) {
	$.ajax({
		url: urlcore + "/api/loanOrder/countPeopleMoney4?gmtDatetime=" + gmtDatetime + "&orderStatus=" + orderStatus + "&name=" + name + "&phoneNumber=" + phoneNumber,
		type: "get",
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		success: function(data) {
			if(data.success == true) {

				$('#totalMoney').text(data.data.totalMoney);
				$('#totalPeople').text(data.data.totalPeople);

				if(data.code == 'OVERTIME') {
					var thisUrl = window.location.href;

					if(thisUrl.indexOf('login.html') <= -1) {
						top.window.location.href = "login.html";
					}

				} else {
					if(data.msg != '空数据') {
						//alert(data.msg)
					} else {
						$('#thiscount').text(0);
					}
				}
			}

		},
		error: function() {
			alert("error");
		}
	});
}


function findMyCatalogue(){
	$.ajax({
		url: urlcore + "/api/roleThirdCatalogue/findAllByUser?secondTitle="+jName,
		type: "GET",
		dataType: 'json',
		async: false,
		contentType: "application/json;charset=utf-8",
		success:function(data){
		if (data.success == true) {
			$.each(data.data, function(i,n) {
				arrayTitle.push(n.thirdCatalogue.title);
			});
		} else {
			alert(data.msg);
		}

		},
		error:function() {
			alert("error");
		}
	});
}
