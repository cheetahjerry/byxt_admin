var gmtDatetime='';
var name='';
var phoneNumber='';
var currentPage=1;
var totalPeople=0;
var totalMoney = 0;
var jName = getCookie('Jname');
//我的权限数组
var arrayTitle = new Array; 
loadMyEssay(gmtDatetime,name,phoneNumber);
countPeopleMoney(gmtDatetime, name, phoneNumber);

function loadMyEssay(gmtDatetime,name,phoneNumber){
	
	$(document).ready(function() {
		findMyCatalogue();
		init(currentPage);
	});

	function init(pageNo) {
		$("#thislist").html("");
		$.ajax({
			url: urlcore + "/api/loanOrder/selectApplyLoanList?gmtDatetime=" + gmtDatetime + "&name=" + name + "&phoneNumber=" + phoneNumber + "&current=" + pageNo,
			type: "get",
			dataType: 'json',
			async:'false',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
        	if(data.success == true) {
					$.each(data.data.list, function(i, n) {
					
						var juese = "新用户";
						if(n.user.isOld == 1){
							
							juese = "老用户";
						}
						var id = n.id;
						var thislist =
							'<tr class="footable-even" style="display: table-row;">' +
							'<td class="footable-visible"><input type="checkbox" /></td>' +
							'<td class="footable-visible">' + n.id + '</td>' +
							'<td class="footable-visible">' + n.orderNumber + '</td>' +
							'<td class="footable-visible">' + n.user.userName + '</td>' +
							'<td class="footable-visible">' + n.user.phone + '</td>' +
							'<td class="footable-visible">' + n.limitDays + '</td>' +
							'<td class="footable-visible">' + n.borrowMoney + '</td>' +
							'<td class="footable-visible">' + n.realMoney + '</td>' +
							'<td class="footable-visible">' + n.needPayMoney + '</td>' +
							'<td class="footable-visible">' + n.gmtDatetime + '</td>' +
							'<td class="footable-visible">' + n.allowDays + '</td>' +
							'<td class="footable-visible">' + juese + '</td>' +
							'<td class="footable-visible footable-last-column">'+
								'<a hidden="hidden" name="查看认证信息" class="" href="tab.html?id=' + n.userId + '&userName=' +escape(n.user.userName)  + '&phone=' + n.user.phone + '" >查看认证信息</a>&nbsp;'+
								'<a hidden="hidden" name="查看老用户" class="" href="javascript:;" data-toggle="modal" data-target="#oldMsg" onclick="selectOld('+n.userId+')">查看老用户</a>&nbsp;'+
								'<a hidden="hidden" name="详情" class="" href="loan_apply_list_detail.html?id='+n.id+'" >详情</a>&nbsp;'+
								'<a hidden="hidden" name="同意" class="" href="javascript:;"  onclick="thisAgree(' + id + ')">同意</a>&nbsp;'+
								'<a hidden="hidden" name="拒绝" class="" href="javascript:;"  onclick="thisRefuse(' + id + ')">拒绝</a>'+
								'</td>' +
							'</tr>';
							//alert(n.userId);
						$('#thislist').append(thislist);
						
					});
					$.each(arrayTitle, function(i,k) {
						$('a[name="'+k+'"]').attr("hidden",false).attr("class","btn btn-primary btn-xs");
					});
					$("#pager").pager({
						pagenumber: pageNo,
						pagecount: data.data.pages,
						totalcount: data.data.total,
						buttonClickCallback: PageClick
					});
				

			      if(data.code == 'OVERTIME') {
					var thisUrl = window.location.href;

					if(thisUrl.indexOf('login.html') <= -1) {
						top.window.location.href = "login.html";
					}

				} else {
					if(data.msg != '空数据') {
						//alert(data.msg)
					} else {
						$('#thiscount').text(0);
					}
				}
				}

			},
			error: function() {
				alert("error");
			}
		});
	}
	PageClick = function(pageclickednumber) {
		init(pageclickednumber);
	}

}
function EnterSearchList() {
	var code = event.keyCode;
	if(code == 13) {
		searchList();
	}
}
function selectOld(userId){
	$.ajax({
			url: urlcore + "/api/loanOrder/selectOldOrder?userId=" + userId,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				$("#zdyuqi").html(data.data.zdyuqi);
				$("#yuqics").html(data.data.yuqics);
				$("#cghuankuan").html(data.data.cghuankuan);
				$("#xqcishu").html(data.data.xqcishu);
			},
			error: function() {
				/* Act on the event */
				alert("error");
			}
	});
}
function searchList(){
	
	var phoneNumber=$('#phoneNumber').val().trim();
	var userName=$('#userName').val().trim();
	var time=$('#applyTime').val().trim();
	loadMyEssay(time,userName,phoneNumber);
	countPeopleMoney(time, userName, phoneNumber);
	
}

function checkAuthDetails(id){
	
}

function thisAgree(id){
	
	if(confirm("您确定同意该申请吗？")) {
		$.ajax({
			url: urlcore + "/api/loanOrder/thisAgree?id=" + id,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				loadMyEssay('');
				countPeopleMoney('')
			},
			error: function() {
				/* Act on the event */
				alert("error");
			}
		});
	}

}
	
	
	


function thisRefuse(id){
	totalMoney = 0;
    totalPeople = 0;
	if(confirm("您确定拒绝该申请吗？")) {
		$.ajax({
			url: urlcore + "/api/loanOrder/thisRefuse?id=" + id,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				loadMyEssay('');
			countPeopleMoney('');
			},
			error: function() {
				/* Act on the event */
				alert("error");
			}
		});
	}
	
}
function countPeopleMoney(gmtDatetime,name, phoneNumber) {
	$.ajax({
		url: urlcore + "/api/loanOrder/countPeopleMoney2?gmtDatetime=" + gmtDatetime + "&name=" + name + "&phoneNumber=" + phoneNumber,
		type: "get",
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		success: function(data) {
			if(data.success == true) {

				$('#totalMoney').text(data.data.totalMoney);
				$('#totalPeople').text(data.data.totalPeople);

				if(data.code == 'OVERTIME') {
					var thisUrl = window.location.href;

					if(thisUrl.indexOf('login.html') <= -1) {
						top.window.location.href = "login.html";
					}

				} else {
					if(data.msg != '空数据') {
						//alert(data.msg)
					} else {
						$('#thiscount').text(0);
					}
				}
			}

		},
		error: function() {
			alert("error");
		}
	});
}

function findMyCatalogue(){
	$.ajax({
		url: urlcore + "/api/roleThirdCatalogue/findAllByUser?secondTitle="+jName,
		type: "GET",
		dataType: 'json',
		async: false,
		contentType: "application/json;charset=utf-8",
		success:function(data){
		if (data.success == true) {
			$.each(data.data, function(i,n) {
				arrayTitle.push(n.thirdCatalogue.title);
			});
		} else {
			alert(data.msg);
		}

		},
		error:function() {
			alert("error");
		}
	});
}
