var gmtDatetime = '';
var name = '';
var phoneNumber = '';
var currentPage = 1;
var totalMoney = 0;
var totalPeople = 0;
var jName = getCookie('Jname');
//我的权限数组
var arrayTitle = new Array;
loadMyEssay(gmtDatetime, name, phoneNumber);
countPeopleMoney(gmtDatetime, name, phoneNumber);

function loadMyEssay(gmtDatetime, name, phoneNumber) {

	$(document).ready(function() {
		findMyCatalogue()
		init(currentPage);
	});

	function init(pageNo) {
		$("#thislist").html("");
		$.ajax({
			url: urlcore + "/api/loanOrder/selectFailApplyList?gmtDatetime=" + gmtDatetime + "&name=" + name + "&phoneNumber=" + phoneNumber + "&current=" + pageNo,
			type: "get",
			async: 'false',
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				if(data.success == true) {
					$.each(data.data.list, function(i, n) {

						var id = n.id;
					

//	var date = n.gmtDatetime.replace("-", "/")
						//alert(date);
						var thislist =
							'<tr class="footable-even" style="display: table-row;">' +
							'<td class="footable-visible">' + n.id + '</td>' +
							'<td class="footable-visible">' + n.orderNumber + '</td>' +
							'<td class="footable-visible">' + n.user.userName + '</td>' +
							'<td class="footable-visible">' + n.user.phone + '</td>' +
							'<td class="footable-visible">' + n.limitDays + '</td>' +
							'<td class="footable-visible">' + n.borrowMoney + '</td>' +
							'<td class="footable-visible">' + n.realMoney + '</td>' +
							'<td class="footable-visible">' + n.needPayMoney + '</td>' +
							'<td class="footable-visible">' + "张天猫" + '</td>' +
							'<td class="footable-visible">' + n.gmtDatetime + '</td>' +
							'<td class="footable-visible footable-last-column">' +
							'<a hidden="hidden" class="" name="查看认证信息" href="tab.html?id=' + n.userId + '&userName=' +escape(n.user.userName)  + '&phone=' + n.user.phone + '" >查看认证信息</a>&nbsp;' +
							'<a hidden="hidden" class="" name="详情" href="apply_fail_list_detail.html?id=' + n.id + '" >详情</a></td>' +
							'</tr>';

						$('#thislist').append(thislist);
					});
					$.each(arrayTitle, function(i, k) {
						$('a[name="' + k + '"]').attr("hidden", false).attr("class", "btn btn-primary btn-xs");
					});
					$("#pager").pager({
						pagenumber: pageNo,
						pagecount: data.data.pages,
						totalcount: data.data.total,
						buttonClickCallback: PageClick
					});

					if(data.code == 'OVERTIME') {
						var thisUrl = window.location.href;

						if(thisUrl.indexOf('login.html') <= -1) {
							top.window.location.href = "login.html";
						}

					} else {
						if(data.msg != '空数据') {
							//alert(data.msg)
						} else {
							$('#thiscount').text(0);
						}
					}
				}

			},
			error: function() {
				alert("error");
			}
		});
	}
	PageClick = function(pageclickednumber) {
		init(pageclickednumber);
	}

}

function EnterSearchList() {
	var code = event.keyCode;
	if(code == 13) {
		searchList();
	}
}

function searchList() {

	var phoneNumber = $('#phoneNumber').val().trim();
	var userName = $('#userName').val().trim();
	var time = $('#applyTime').val().trim();
	loadMyEssay(time, userName, phoneNumber);
	countPeopleMoney(time, userName, phoneNumber);

}

function countPeopleMoney(gmtDatetime, name, phoneNumber) {
	$.ajax({
		url: urlcore + "/api/loanOrder/countPeopleMoney1?gmtDatetime=" + gmtDatetime + "&name=" + name + "&phoneNumber=" + phoneNumber,
		type: "get",
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		success: function(data) {
			if(data.success == true) {

				$('#totalMoney').text(data.data.totalMoney);
				$('#totalPeople').text(data.data.totalPeople);

				if(data.code == 'OVERTIME') {
					var thisUrl = window.location.href;

					if(thisUrl.indexOf('login.html') <= -1) {
						top.window.location.href = "login.html";
					}

				} else {
					if(data.msg != '空数据') {
						//alert(data.msg)
					} else {
						$('#thiscount').text(0);
					}
				}
			}

		},
		error: function() {
			alert("error");
		}
	});
}

function findMyCatalogue() {
	$.ajax({
		url: urlcore + "/api/roleThirdCatalogue/findAllByUser?secondTitle=" + jName,
		type: "GET",
		dataType: 'json',
		async: false,
		contentType: "application/json;charset=utf-8",
		success:function(data){
		if (data.success == true) {
			$.each(data.data, function(i,n) {
				arrayTitle.push(n.thirdCatalogue.title);
			});
		} else {
			alert(data.msg);
		}

		},
		error: function() {
			alert("error");
		}
	});
}

/*function myBrowser() {
	var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串
	var isOpera = userAgent.indexOf("Opera") > -1;
	if(isOpera) {
		return "Opera"
	}; //判断是否Opera浏览器
	if(userAgent.indexOf("Firefox") > -1) {
		return "FF";
	} //判断是否Firefox浏览器
	if(userAgent.indexOf("Chrome") > -1) {
		return "Chrome";
	}
	if(userAgent.indexOf("Safari") > -1) {
		return "Safari";
	} //判断是否Safari浏览器
	if(userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera) {
		return "IE";
	}; //判断是否IE浏览器
}
//以下是调用上面的函数
var mb = myBrowser();
if("IE" == mb) {
	alert("我是 IE");
}
if("FF" == mb) {
	alert("我是 Firefox");
}
if("Chrome" == mb) {
	alert("我是 Chrome");
}
if("Opera" == mb) {
	alert("我是 Opera");
}
if("Safari" == mb) {
	alert("我是 Safari");
}*/