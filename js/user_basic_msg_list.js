var phone="";
var realName="";
var status = -1;
var jName = getCookie('Jname');
//我的权限数组
var arrayTitle = new Array; 
$(function(){
	searchuser();
});


function loadMyEssay() {		
	$(document).ready(function() {
		findMyCatalogue()
		//设置默认第1页
	    init(1);
	});
	
	//默认加载  
	function init(pageNo){
		//获取用户信息列表
		$("#thislist").html("");
		$.ajax({
			url: urlcore + "/api/userBasicMsg/findByPage?&pageNo="+pageNo+"&pageSize="+pageSize+"&phone="+phone+"&status="+status+"&realName="+realName,
			type: "get",
			acsny:false,
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success:function(data){
				if (data.success == true) {
					//i表示在data中的索引位置，n表示包含的信息的对象
					$.each(data.data.list,function(i,n){
						var statusTitle;
						if (n.status == 0) {
							statusTitle = "未认证";
						}else if(n.status == 1){
							statusTitle = "认证成功";
						}else if(n.status == 2){
							statusTitle = "认证失败";
						}
						var gmtDatetime = new Date(n.gmtDatetime).pattern("yyyy-MM-dd hh:mm:ss");
						var thislist = '<tr>'+
'                                    <td class="footable-visible">'+n.id+'</td>'+
'                                    <td class="footable-visible"><img width="40px" height="40px" src="'+n.user.headImg+'"></td>'+
'                                    <td class="footable-visible">'+n.user.userName+'</td>'+
'                                    <td class="footable-visible">'+n.user.phone+'</td>'+
//'                                    <td class="footable-visible">'+n.user.authScore+'</td>'+
'                                    <td class="footable-visible">'+n.user.identityNum+'</td>'+
'                                    <td class="footable-visible">'+n.user.wxId+'</td>'+
'                                    <td class="footable-visible">'+gmtDatetime+'</td>'+
'                                    <td class="footable-visible">'+statusTitle+'</td>'+
'                                    <td class="footable-visible footable-last-column">';
										if (n.status == 0 || n.status == 2) { 
											thislist+='<a name="认证" hidden="hidden" class="" href="authentication.html?id='+n.id+'&userId='+n.user.id+'&status='+n.status+'" >认证</a>';
										}
										thislist+=
'                                        <a name="详情" hidden="hidden" class="" href="authentication.html?id='+n.id+'&userId='+n.user.id+'&status='+n.status+'" >详情</a>'+
// '                                        <a name="手机通讯录" hidden="hidden" class="" href="user_phone_list.html?userId='+n.user.id+'" >手机通讯录</a>'+
'                                    </td>'+
'                                </tr>';
						$('#thislist').append(thislist);
					});
					$.each(arrayTitle, function(i,k) {
						$('a[name="'+k+'"]').attr("hidden",false).attr("class","btn btn-primary btn-xs");
					});
					$('#thiscount').text(data.data.total);
					$("#pager").pager({
					pagenumber: pageNo, 
					pagecount:data.data.pages,
					totalcount:data.data.total,
					buttonClickCallback: PageClick
					}); 
					
				} else if (data.code == 'OVERTIME'){
					var thisUrl = window.location.href;
					if (thisUrl.indexOf('login.html') <= -1) {
						top.window.location.href="login.html";
					}

				} else {
					if (data.msg != '空数据') {
						alert(data.msg)
					}else{
						$('#thiscount').text(0);
					}
				}

			},
			error:function() {
				/* Act on the event */
				alert("error");
			}
		});
	}

	//回调函数  
	PageClick = function(pageclickednumber) {  
	    init(pageclickednumber); 
	}

}

//搜索
function searchuser() {
	phone = $('#phone').val().trim();
	realName = $('#realName').val().trim();
	status = $('#status').val();
	loadMyEssay();
}

//
function thisUpdate(id) {	
	$('#thisId_update_fail').val(id);
}



//修改
function thisStatus(id) {
	$.ajax({
		url: urlcore + "/api/userRealName/updateStatus?id="+id+"&status="+1,
		type: "get",
		dataType: 'json',		
		contentType: "application/json;charset=utf-8",
		success:function(data){
			if (data.success == true) {
				var da = data.data;				
					loadMyEssay('',0);
			} else if (data.code == 'OVERTIME'){
				var thisUrl = window.location.href;
				if (thisUrl.indexOf('login.html') <= -1) {
					top.window.location.href="login.html";
				}

			} else {
				alert(data.msg);
			}

		},
		error:function() {
			/* Act on the event */
			alert("error");
		}
	});	
}



//填写认证不通过备注并提交
function toUpdateFail() {
	var id = $('#thisId_update_fail').val();
	
	var remark = $('#remark_update_fail').val().trim();	
	$.ajax({
		url: urlcore + "/api/userRealName/updateReason?id="+id+"&reason="+remark,
		type: "get",
		dataType: 'json',		
		contentType: "application/json;charset=utf-8",
		success:function(data){
			if (data.success == true) {
				var da = data.data;				
					loadMyEssay('',0);
			} else if (data.code == 'OVERTIME'){
				var thisUrl = window.location.href;
				if (thisUrl.indexOf('login.html') <= -1) {
					top.window.location.href="login.html";
				}

			} else {
				alert(data.msg);
			}

		},
		error:function() {
			/* Act on the event */
			alert("error");
		}
	});	
}

function findMyCatalogue(){
	$.ajax({
		url: urlcore + "/api/roleThirdCatalogue/findAllByUser?secondTitle="+jName,
		type: "GET",
		dataType: 'json',
		async: false,
		contentType: "application/json;charset=utf-8",
		success:function(data){
		if (data.success == true) {
			$.each(data.data, function(i,n) {
				arrayTitle.push(n.thirdCatalogue.title);
			});
		} else {
			alert(data.msg);
		}

		},
		error:function() {
			alert("error");
		}
	});
}