
var phone = getvl('phone');
var userName = getvl('userName');
var id = getvl("id")


loadMyEssay(id);
loadMyEssay5(id);




function loadMyEssay(id) {

	$.ajax({
		url: urlcore + "/api/userIdentity/selectOneDetailsByUserId?id=" + id,
		type: "get",
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		success: function(data) {
			if(data.success == true) {
				var da = data.data;
				var status = '';
				if(da.status == 0) {
					status = "未认证"
				} else if(da.status == 1) {
					status = "已认证"
				}

				$('#userName').html(da.userName);
				$('#phoneNumber').html(da.user.phone);
				$('#identityNum').html(da.identityNum);
				$('#status').html(status);
				var s = new Date(da.gmtDatetime).pattern("yyyy-MM-dd hh:mm:ss");
				$('#gmtDatetime111').html(s);
				document.getElementById("bigidentityFront").rel = da.identityFront;

                document.getElementById("bigidentityBack").rel = da.identityBack;
                document.getElementById("identityFront").src = da.identityFront;
				document.getElementById("identityBack").src = da.identityBack;

				document.getElementById("bigfaceUrl").rel = da.faceUrl;
				document.getElementById("faceUrl").src = da.faceUrl;

			}

		},
		error: function() {
			/* Act on the event */
			alert("error");
		}
	});

}

function loadMyEssay5(id) {
	$.ajax({
		url: urlcore + "/api/userBasicMsg/selectOneDetailsByUserId?id=" + id,
		type: "get",
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		success: function(data) {
			if(data.success == true) {
				var n = data.data;
				
				$("#phone").html(phone);
			
				$("#realName").html(userName);
				$("#gmtDatetime").html(n.gmtDatetime);
				$("#score").html(n.user.authScore);
				$("#marry").html(n.marry);
				$("#study").html(n.study);
				$("#province").html(n.province);
				$("#city").html(n.city);
				$("#county").html(n.county);
				$("#areaCode").html(n.areaCode);
				$("#addressDetail").html(n.addressDetails);
				$("#workCompany").html(n.workCompany);
				$("#workPlace").html(n.workPlace);
				$("#workMoney").html(n.workMoney);
				$("#workPhone").html(n.workPhone);



				$("#idNo").html(n.userIdentity.identityNum);
				$("#wxId").html(n.user.wxId);
                document.getElementById("bigidentityF").rel = n.userIdentity.identityFront;

                document.getElementById("bigidentityB").rel = n.userIdentity.identityBack;
				$("#identityF").attr("src",n.userIdentity.identityFront)
				$("#identityB").attr("src",n.userIdentity.identityBack)

				
				$("#linkPersonNameOne").html(n.linkPersonNameOne);
				$("#linkPersonPhoneOne").html(n.linkPersonPhoneOne);
				$("#linkPersonRelationOne").html(n.linkPersonRelationOne);
				$("#linkPersonNameTwo").html(n.linkPersonNameTwo);
				$("#linkPersonPhoneTwo").html(n.linkPersonPhoneTwo);
				$("#linkPersonRelationTwo").html(n.linkPersonRelationTwo);

			} else if(data.code == 'OVERTIME') {
				var thisUrl = window.location.href;
				if(thisUrl.indexOf('login.html') <= -1) {
					top.window.location.href = "login.html";
				}

			} else if(data.code == 'PARAMETER_INVALID') {
				var thisUrl = window.location.href;
				if(thisUrl.indexOf('login.html') <= -1) {
					top.window.location.href = "login.html";
				}
			} else {
				alert(data.msg);
			}

		},
		error: function() {
			/* Act on the event */
			alert("error");
		}
	});
}



function loadMyEssay7() {
	$.ajax({
		url: urlcore + "/api/userBank/selectByUserId?id=" + id,
		type: "get",
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		success: function(data) {
			if(data.success == true) {
				var n = data.data;
				$("#bankcardno").html(n.bankcardno);
				$("#name").html(n.name);
				$("#idcardno").html(n.idcardno);
				$("#bankPhone").html(n.bankPhone);

			} else if(data.code == 'OVERTIME') {
				var thisUrl = window.location.href;
				if(thisUrl.indexOf('login.html') <= -1) {
					top.window.location.href = "login.html";
				}

			} else if(data.code == 'PARAMETER_INVALID') {
				var thisUrl = window.location.href;
				if(thisUrl.indexOf('login.html') <= -1) {
					top.window.location.href = "login.html";
				}
			} else {
				alert(data.msg);
			}

		},
		error: function() {
			/* Act on the event */
			alert("error");
		}
	});
}





function loadMyEssay1() {
	$(document).ready(function() {
		//设置默认第1页
		init(1);
		base();
		
	});

	function base(){
		$.ajax({
			url: urlcore + "/api/userTaobaoAddress/findUserTBBaseInfo?userId=" + id,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				if(data.success == true) {
					var da = data.data;
					$("#tb_userName").html(da.userName);
					$("#tb_nickName").html(da.nickName);
					$("#tb_name").html(da.name);
					$("#tb_gender").html(da.gender);
					$("#tb_mobile").html(da.mobile);
					$("#tb_realName").html(da.realName);
				} else if(data.code == 'OVERTIME') {
					var thisUrl = window.location.href;
					if(thisUrl.indexOf('login.html') <= -1) {
						top.window.location.href = "login.html";
					}

				} else {
					if(data.msg != '空数据') {
						alert(data.msg)
					} else {
						$('#thiscount').text(0);
					}
				}

			},
			error: function() {
				/* Act on the event */
				alert("error");
			}
		});
	}
	

	//默认加载  
	function init(pageNo) {
		//获取用户信息列表
		$("#thislist1").html("");
		$.ajax({
			url: urlcore + "/api/userTaobaoAddress/findByAddressPage?pageNo=" + pageNo + "&pageSize=" + pageSize + "&userId=" + id,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				if(data.success == true) {
					//i表示在data中的索引位置，n表示包含的信息的对象
					$.each(data.data.list, function(i, n) {
						var isDefault = "";
						if("1" == n.defaultArea){
							isDefault = "默认地址";
						}
						var thislist1 = '<tr>' +
							'<td class="footable-visible">' + n.id + '</td>' +
							'<td class="footable-visible">' + n.name + '</td>' +
							'<td class="footable-visible">' + n.area + '</td>' +
							'<td class="footable-visible">' + n.address + '</td>' +
							'<td class="footable-visible">' + n.mobile + '</td>' +
							'<td class="footable-visible">' + isDefault + '</td>' +
							'</tr>';
						$('#thislist1').append(thislist1);
					});

					$("#pager1").pager({
						pagenumber: pageNo,
						pagecount: data.data.pages,
						totalcount: data.data.total,
						buttonClickCallback: PageClick
					});

				} else if(data.code == 'OVERTIME') {
					var thisUrl = window.location.href;
					if(thisUrl.indexOf('login.html') <= -1) {
						top.window.location.href = "login.html";
					}

				} else {
					if(data.msg != '空数据') {
						alert(data.msg)
					} else {
						$('#thiscount').text(0);
					}
				}

			},
			error: function() {
				/* Act on the event */
				alert("error");
			}
		});
	}

	//回调函数  
	PageClick = function(pageclickednumber) {
		init(pageclickednumber);
	}

}

/*function loadMyEssay2() {
	$(document).ready(function() {
		//设置默认第1页
		init(1);
	});

	//默认加载
	function init(pageNo) {
		//获取用户信息列表
		$("#thislist2").html("");
		$.ajax({
			url: urlcore + "/api/userJindongAddress/findByAddressPage?pageNo=" + pageNo + "&pageSize=" + pageSize + "&userId=" + id,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				if(data.success == true) {
					//i表示在data中的索引位置，n表示包含的信息的对象
					$.each(data.data.list, function(i, n) {
						var thislist2 = '<tr>' +
							'                                    <td class="footable-visible">' + n.id + '</td>' +
							'                                    <td class="footable-visible">' + phone + '</td>' +
							'                                    <td class="footable-visible">' + userName + '</td>' +
							'                                    <td class="footable-visible">' + n.orderAddress + '</td>' +
							'                                </tr>';
						$('#thislist2').append(thislist2);
					});

					$("#pager2").pager({
						pagenumber: pageNo,
						pagecount: data.data.pages,
						totalcount: data.data.total,
						buttonClickCallback: PageClick
					});

				} else if(data.code == 'OVERTIME') {
					var thisUrl = window.location.href;
					if(thisUrl.indexOf('login.html') <= -1) {
						top.window.location.href = "login.html";
					}

				} else {
					if(data.msg != '空数据') {
						alert(data.msg)
					} else {
						$('#thiscount').text(0);
					}
				}

			},
			error: function() {
				/!* Act on the event *!/
				alert("error");
			}
		});
	}

	//回调函数
	PageClick = function(pageclickednumber) {
		init(pageclickednumber);
	}

}*/

function loadMyEssay2() {
    $(document).ready(function() {
        //设置默认第1页
        init(1);
    });

    //默认加载
    function init(pageNo) {
        //获取用户信息列表
        $("#thislist2").html("");
        $.ajax({
            url: urlcore + "/api/userJindongAddress/findByAddressPage?pageNo=" + pageNo + "&pageSize=" + pageSize + "&userId=" + id,
            type: "get",
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            success: function(data) {
                if(data.success == true) {
                    //i表示在data中的索引位置，n表示包含的信息的对象
                    $.each(data.data.list, function(i, n) {
                        var thislist2 = '<tr>' +
                            '                                    <td class="footable-visible">' + n.id + '</td>' +
                            '                                    <td class="footable-visible">' + phone + '</td>' +
                            '                                    <td class="footable-visible">' + userName + '</td>' +
                            '                                    <td class="footable-visible">' + n.orderAddress + '</td>' +
                            '                                </tr>';
                        $('#thislist2').append(thislist2);
                    });

                    $("#pager2").pager({
                        pagenumber: pageNo,
                        pagecount: data.data.pages,
                        totalcount: data.data.total,
                        buttonClickCallback: PageClick
                    });

                } else if(data.code == 'OVERTIME') {
                    var thisUrl = window.location.href;
                    if(thisUrl.indexOf('login.html') <= -1) {
                        top.window.location.href = "login.html";
                    }

                } else {
                    if(data.msg != '空数据') {
                        alert(data.msg)
                    } else {
                        $('#thiscount').text(0);
                    }
                }

            },
            error: function() {
                /* Act on the event */
                alert("error");
            }
        });
    }

    //回调函数
    PageClick = function(pageclickednumber) {
        init(pageclickednumber);
    }

}

function loadMyEssay3() {
	$(document).ready(function() {
		//设置默认第1页
		init(1);
	});

	//默认加载  
	function init(pageNo) {
		//获取用户信息列表
		$("#thislist3").html("");
		$.ajax({
			url: urlcore + "/api/userPhoneList/findByUserPage?userId=" + id + "&pageNo=" + pageNo + "&pageSize=" + pageSize,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				if(data.success == true) {
					//i表示在data中的索引位置，n表示包含的信息的对象
					$.each(data.data.list, function(i, n) {
						var thislist3 = '<tr>' +
							'                                    <td class="footable-visible">' + n.id + '</td>' +
							'                                    <td class="footable-visible">' + n.name + '</td>' +
							'                                    <td class="footable-visible">' + n.phone + '</td>' +
							'                                    <td class="footable-visible">' + n.link + '</td>' +
							'                                </tr>';
						$('#thislist3').append(thislist3);
					});

					$("#pager3").pager({
						pagenumber: pageNo,
						pagecount: data.data.pages,
						totalcount: data.data.total,
						buttonClickCallback: PageClick
					});

				} else if(data.code == 'OVERTIME') {
					var thisUrl = window.location.href;
					if(thisUrl.indexOf('login.html') <= -1) {
						top.window.location.href = "login.html";
					}

				} else {
					if(data.msg != '空数据') {
						alert(data.msg)
					} else {
						$('#thiscount').text(0);
					}
				}

			},
			error: function() {
				/* Act on the event */
				alert("error");
			}
		});
	}

	//回调函数  
	PageClick = function(pageclickednumber) {
		init(pageclickednumber);
	}

}

function loadMyEssay4() {
//	$(document).ready(function() {
//		//设置默认第1页
//		init(1);
//	});

//	//默认加载  
//	function init(pageNo) {
//		//获取用户信息列表
//		$("#thislist4").html("");
//		$.ajax({
//			url: urlcore + "/api/userPhoneRecord/findByUserIdPage?userId=" + id + "&pageNo=" + pageNo + "&pageSize=" + pageSize,
//			type: "get",
//			dataType: 'json',
//			contentType: "application/json;charset=utf-8",
//			success: function(data) {
//				if(data.success == true) {
//					//i表示在data中的索引位置，n表示包含的信息的对象
//					$.each(data.data.list, function(i, n) {
//						var thislist4 = '<tr>' +
//							'                                    <td class="footable-visible">' + n.phoneNo + '</td>' +
//							'                                    <td class="footable-visible">' + n.connTimes + '</td>' +
//							'                                    <td class="footable-visible">' + n.startTime + '</td>' +
//							'                                    <td class="footable-visible">' + n.commPlac + '</td>' +
//							'                                    <td class="footable-visible">' + n.callType + '</td>' +
//							'                                    <td class="footable-visible">' + n.commMode + '</td>' +
//							'                                    <td class="footable-visible">' + n.commFee + '</td>' +
//							'                                </tr>';
//						$('#thislist4').append(thislist4);
//					});
//
//					$("#pager4").pager({
//						pagenumber: pageNo,
//						pagecount: data.data.pages,
//						totalcount: data.data.total,
//						buttonClickCallback: PageClick
//					});
//
//				} else if(data.code == 'OVERTIME') {
//					var thisUrl = window.location.href;
//					if(thisUrl.indexOf('login.html') <= -1) {
//						top.window.location.href = "login.html";
//					}
//
//				} else {
//					if(data.msg != '空数据') {
//						alert(data.msg)
//					} else {
//						$('#thiscount').text(0);
//					}
//				}
//
//			},
//			error: function() {
//				/* Act on the event */
//				alert("error");
//			}
//		});
//	}
//
//	//回调函数  
//	PageClick = function(pageclickednumber) {
//		init(pageclickednumber);
//	}
	var taskId = "";
	var userName = "";
	var new_window = window.open("","","width=1000,height=800");
	$.ajax({
			url: urlcore + "/api/user/selectYYSResult?userId=" + id ,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				taskId = data.data.taskId;
				userName = data.data.mobile;
				new_window .location = "https://portal.shujumohe.com/main/customerReport/"+taskId+"?user_name="+userName;
			},
			error: function() {
				
				alert("error");
			}
		});
//	myWindow=window.open("https://portal.shujumohe.com/main/customerReport/"+taskId+"?user_name="+userName,'','width=1000,height=800');
//	myWindow.focus();
}

function loadMyEssay6() {
	$(document).ready(function() {
		//设置默认第1页
		init(1);
	});

	//默认加载  
	function init(pageNo) {
		//获取用户信息列表
		$("#thislist6").html("");
		$.ajax({
			url: urlcore + "/api/userTaobaoZhifubao/findByUserPage?userId=" + id + "&pageNo=" + pageNo + "&pageSize=" + pageSize,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				if(data.success == true) {
					//i表示在data中的索引位置，n表示包含的信息的对象
					$.each(data.data.list, function(i, n) {
						var thislist3 = '<tr>' +
							'                                    <td class="footable-visible">' + n.id + '</td>' +
							'                                    <td class="footable-visible">' + userName + '</td>' +
							'                                    <td class="footable-visible">' + n.huabeiCanUseMoney + '</td>' +
							'                                    <td class="footable-visible">' + n.huabeiTotalAmount + '</td>' +
							'                                    <td class="footable-visible">' + n.alipayRemainingAmount + '</td>' +
						
							'                                </tr>';
						$('#thislist6').append(thislist3);
					});

					$("#pager6").pager({
						pagenumber: pageNo,
						pagecount: data.data.pages,
						totalcount: data.data.total,
						buttonClickCallback: PageClick
					});

				} else if(data.code == 'OVERTIME') {
					var thisUrl = window.location.href;
					if(thisUrl.indexOf('login.html') <= -1) {
						top.window.location.href = "login.html";
					}

				} else {
					if(data.msg != '空数据') {
						alert(data.msg)
					} else {
						$('#thiscount').text(0);
					}
				}

			},
			error: function() {
				
				alert("error");
			}
		});
	}

	//回调函数  
	PageClick = function(pageclickednumber) {
		init(pageclickednumber);
	}

}

//function test(){
//             
//			var  td_data= null;
//			
//
//     		var  person_info= null;
//     		
//     		$.ajax({
//			url: urlcore + "/api/mobileAuthentication/query?userId ="+id,
//			type: "get",
//			dataType: 'json',
//			contentType: "application/json;charset=utf-8",
//			success: function(data) {
//				if(data.success == true) {
//					
//					td_data = data.data.td_data;
//					person_info = data.data.person_info;
//					
//
//			},
//			error: function() {
//				/* Act on the event */
//				alert("error");
//			}
//			});
//     		alert(td_data);
//     		alert(person_info);
//     		$.showTDReport(td_data,person_info)
//}
        
        function test()
        {
             
			var  td_data= null;

       		var  person_info=null;
       		$.ajax({
				url: urlcore + "/api/mobileAuthentication/query?userId="+id,
				type: "get",
				dataType: 'json',
				contentType: "application/json;charset=utf-8",
				success: function(data) {
					if(data.success == true) {
						if("undefined" == typeof(data.data.td_data1)){
							alert("同盾分为"+data.data.score);
						}else{
							td_data = data.data.td_data1;
							person_info = data.data.person_info1;
	       					$.showTDReport(td_data,person_info)
						}
						
					}
				},
				error: function() {
					/* Act on the event */
					alert("error");
				}
				});
			
       		
        }
        
        (function(){
            if(typeof jQuery == "undefined"){
                var  jq_script = document.createElement('script');
                jq_script.type = "text/javascript";
                jq_script.src =  "http://lib.sinaapp.com/js/jquery/1.9.1/jquery-1.9.1.min.js";
                jq_script.onload = loadPreloanLib;
                document.getElementsByTagName('head')[0].appendChild(jq_script);
            } else {
                loadPreloanLib();
            }
         
         
            function loadPreloanLib(){
                var td_script = document.createElement('script');
                td_script.type = "text/javascript";
                td_script.src = "http://cdnjs.tongdun.cn/preloan/tdreport.1.4.min.js?r=" + (new Date()).getTime();
                document.getElementsByTagName('head')[0].appendChild(td_script);
            }
		})();


        