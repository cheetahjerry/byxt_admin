var gmtDatetime = '';
var name = '';
var phoneNumber = '';
var currentPage = 1;
var temp=0;
var totalMoney = 0;
var totalPeople = 0;
var jName = getCookie('Jname');
//我的权限数组
var arrayTitle = new Array; 
loadMyEssay(gmtDatetime, name, phoneNumber);
countPeopleMoney(gmtDatetime, name, phoneNumber);

function loadMyEssay(gmtDatetime, name, phoneNumber) {
	
	$(document).ready(function() {
		findMyCatalogue();
		init(currentPage);
	});

	function init(pageNo) {
		$("#thislist").html("");
		$.ajax({
			url: urlcore + "/api/loanOrder/toPassMoney?gmtDatetime=" + gmtDatetime + "&name=" + name + "&phoneNumber=" + phoneNumber + "&current=" + pageNo,
			type: "get",
			async: 'false',
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				if(data.success == true) {

					$.each(data.data.list, function(i, n) {

						var id = n.id;
						var thislist =
							'<tr class="footable-even" style="display: table-row;">' +
							'<td class="footable-visible"><input type="checkbox" value="'+n.id+'" name="selectcheck" /></td>' +
							'<td class="footable-visible">' + n.id + '</td>' +
							'<td class="footable-visible">' + n.orderNumber + '</td>' +
							'<td class="footable-visible">' + n.user.userName + '</td>' +
							'<td class="footable-visible">' + n.user.phone + '</td>' +
							'<td class="footable-visible">' + n.limitDays + '</td>' +
							'<td class="footable-visible">' + n.borrowMoney + '</td>' +
							'<td class="footable-visible">' + n.realMoney + '</td>' +
							'<td class="footable-visible">' + n.needPayMoney + '</td>' +
							'<td class="footable-visible">' + "李浩" + '</td>' +
							
							'<td class="footable-visible">' +n.gmtDatetime + '</td>' +
							'<td class="footable-visible footable-last-column">'+
								'<a hidden="hidden" name="查看认证信息" class="" href="tab.html?id='+n.userId+'&userName='+escape(n.user.userName)+'&phone='+n.user.phone+'" >查看认证信息</a>&nbsp;'+
								'<a hidden="hidden" name="详情" class=""href="to_passmoney_list_detail.html?id=' + n.id + '">详情</a>&nbsp;'+
								'<a hidden="hidden" name="打款" class="" href="javascript:;"  onclick="passMoney(' + id + ')">打款</a>&nbsp;'+
								'<a hidden="hidden" name="拒绝" class="" href="javascript:;"  onclick="thisRefuse(' + id + ')">拒绝</a>'+
							'</td>' +
							'</tr>';
						$('#thislist').append(thislist);
						temp+=1;
					
					});
					$.each(arrayTitle, function(i,k) {
						$('a[name="'+k+'"]').attr("hidden",false).attr("class","btn btn-primary btn-xs");
						$('a[data="'+k+'"]').attr("class","btn btn-sm btn-primary");
					});
					$("#pager").pager({
						pagenumber: pageNo,
						pagecount: data.data.pages,
						totalcount: data.data.total,
						buttonClickCallback: PageClick
					});
				

					if(data.code == 'OVERTIME') {
						var thisUrl = window.location.href;

						if(thisUrl.indexOf('login.html') <= -1) {
							top.window.location.href = "login.html";
						}

					} else {
						if(data.msg != '空数据') {
							//alert(data.msg)
						} else {
							$('#thiscount').text(0);
						}
					}
				}

			},
			error: function() {
				alert("error");
			}
		});
	}
	PageClick = function(pageclickednumber) {
		init(pageclickednumber);
	}

}

function EnterSearchList() {
	var code = event.keyCode;
	if(code == 13) {
		searchList();
	}
}

function searchList() {
  
	var phoneNumber = $('#phoneNumber').val().trim();
	var userName = $('#userName').val().trim();
	var time = $('#applyTime').val().trim();
	loadMyEssay(time, userName, phoneNumber);
	countPeopleMoney(time, userName, phoneNumber);
}

function checkAuthDetails(id) {

}



function passMoney(id) {
	
	if(confirm("您确定打款吗？")) {
		$.ajax({
			url: urlcore + "/api/user/payOrder1?id=" + id,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				alert(data.msg);
				loadMyEssay('','','');
				countPeopleMoney('','','');
				location.reload()
			},
			error: function() {
				/* Act on the event */
				console.log(data.msg);
				alert(data.msg);
			}
		});
	}


}


function passSelected(){
	
	if(temp==0) {
		alert("当前无未打款订单！")
	} else {
		 if(confirm("您确定一键全部打款吗？")) {
			var obj = document.getElementsByName("selectcheck");
			for(var k in obj){
				if(obj[k].checked){
					
					passMoney1(obj[k].value);
				}
				
		}
			loadMyEssay('','',''); 
		
 }
}
	
}

function passMoney1(id) {
	
		$.ajax({
			url: urlcore + "/api/loanOrder/passMoney?id=" + id,
			type: "get",
			async: 'false',
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				countPeopleMoney('','','');
				
			},
			error: function() {
				/* Act on the event */
				alert("error");
			}
		});
    
  
}

//
//function passAllMoney() {
//	totalMoney = 0;
//  totalPeople = 0;
//	if(temp==0) {
//		alert("当前无未打款订单！")
//	} else {
//		if(confirm("您确定一键全部打款吗？")) {
//			var obj = document.getElementsByName("selectcheck");
//			for(var k in obj){
//				if(obj[k].checked){
//					
//				}
//			}
//			
//		$.ajax({
//			url: urlcore + "/api/loanOrder/passAllMoney",
//			type: "get",
//			dataType: 'json',
//			contentType: "application/json;charset=utf-8",
//			success: function(data) {
//				alert("打款成功!")
//				loadMyEssay('','','');
//			},
//			error: function() {
//				/* Act on the event */
//				alert("error");
//			}
//		});
//	}
// }
//}

function thisRefuse(id) {
	
	if(confirm("您确定拒绝该申请吗？")) {
		$.ajax({
			url: urlcore + "/api/loanOrder/thisRefuse?id=" + id,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				loadMyEssay('','','');
				countPeopleMoney('','','');
			   
			},
			error: function() {
				/* Act on the event */
				alert("error");
			}
		});
	}
 
}

function selectAll(o){
	var mm=document.getElementsByName("selectcheck");
	for(var i=0;i<mm.length;i++){
		mm[i].checked=o.checked;
	}
}

function countPeopleMoney(gmtDatetime,name, phoneNumber) {
	$.ajax({
		url: urlcore + "/api/loanOrder/countPeopleMoney3?gmtDatetime=" + gmtDatetime + "&name=" + name + "&phoneNumber=" + phoneNumber,
		type: "get",
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		success: function(data) {
			if(data.success == true) {

				$('#totalMoney').text(data.data.totalMoney);
				$('#totalPeople').text(data.data.totalPeople);

				if(data.code == 'OVERTIME') {
					var thisUrl = window.location.href;

					if(thisUrl.indexOf('login.html') <= -1) {
						top.window.location.href = "login.html";
					}

				} else {
					if(data.msg != '空数据') {
						//alert(data.msg)
					} else {
						$('#thiscount').text(0);
					}
				}
			}

		},
		error: function() {
			alert("error");
		}
	});
}

function findMyCatalogue(){
	$.ajax({
		url: urlcore + "/api/roleThirdCatalogue/findAllByUser?secondTitle="+jName,
		type: "GET",
		dataType: 'json',
		async: false,
		contentType: "application/json;charset=utf-8",
		success:function(data){
		if (data.success == true) {
			$.each(data.data, function(i,n) {
				arrayTitle.push(n.thirdCatalogue.title);
			});
		} else {
			alert(data.msg);
		}

		},
		error:function() {
			alert("error");
		}
	});
}
