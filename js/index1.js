var time = '';
$(document).ready(function() {
	addYears();
	allWateMoney();
	init();
});
	 
//默认加载  
function init(){
	//获取信息列表
	$.ajax({ 
		url: urlcore + "/api/index/statementSelect",
		type: "get",
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		success:function(data){
			if (data.success == true) {
				$('#userAddCount').text(data.data.userAddCount == null ? 0 : data.data.userAddCount);
				$('#moneyOutAll').text(data.data.moneyOutAll == null ? 0 : data.data.moneyOutAll);
				$('#moneyBackAll').text(data.data.moneyBackAll == null ? 0 : data.data.moneyBackAll);
				$('#orderPassUserCount').text(data.data.orderPassUserCount == null ? 0 : data.data.orderPassUserCount);
				$('#allCount').text(data.data.personRecord.allCount);
				$('#outOrderCount').text(data.data.personRecord.outOrderCount)
				$('#badOrderCount').text(data.data.personRecord.badOrderCount)
				$('#overOrderCount').text(data.data.personRecord.overOrderCount)
				$('#blackCount').text(data.data.personRecord.blackCount)
				$('#outMoney').text(data.data.personRecord.outMoney)
				$('#memberCount').text(data.data.personRecord.memberCount)
				init1();
				
			} else if (data.code == 'OVERTIME'){
				var thisUrl = window.location.href;
				if (thisUrl.indexOf('login.html') <= -1) {
					top.window.location.href="login.html";
				}

			} else {
				if (data.msg != '空数据') {
					// alert(data.msg)
				}else{
					$('#thiscount').text(0);
				}
			}

		},
		error:function() {
			alert("error");
		}
	});
}
//回调函数  
PageClick = function(pageclickednumber) {  
    init(pageclickednumber); 
}
function searchList() {
	var time = $('#time').val().trim();
	selectSB(time);
}
function selectSB(time){
	$.ajax({ 
		url: urlcore + "/api/index/selectSB?time="+time,
		type: "get",
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		success:function(data){
			$('#allMoney').text(data.data.allMoney);
			$('#allBenJin').text(data.data.allBenJin);
			$('#allLiXi').text(data.data.allLiXi);
			$('#allXuQi').text(data.data.allXuQi);
		},
		error:function() {
			alert("error");
		}
	});
}
function init1(){
	
	var year = $('#year').val();
	
	//获取信息列表
	$.ajax({ 
		url: urlcore + "/api/index/findMoneyStatistics?year="+year,
		type: "get",
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		async: false,
		success:function(data){
			if (data.success == true) {
				
			outMoneys = data.data.outMoneys;
			backMoneys = data.data.backMoneys;
			var first = outMoneys[0];
			var second = outMoneys[1];
			var third = outMoneys[2];
			var fourth = outMoneys[3];
			var fifth = outMoneys[4];
			var sixth = outMoneys[5];
			var seventh = outMoneys[6];
			var eighth = outMoneys[7];
			var ninth = outMoneys[8];
			var tenth = outMoneys[9];
			var eleven = outMoneys[10];
			var twelve = outMoneys[11];
			
			var firsts = backMoneys[0];
			var seconds = backMoneys[1];
			var thirds = backMoneys[2];
			var fourths = backMoneys[3];
			var fifths = backMoneys[4];
			var sixths = backMoneys[5];
			var sevenths = backMoneys[6];
			var eighths = backMoneys[7];
			var ninths = backMoneys[8];
			var tenths = backMoneys[9];
			var elevens = backMoneys[10];
			var twelves = backMoneys[11];
			
			
			var myChart = echarts.init(document.getElementById('main'));
			// 指定图表的配置项和数据
		    option = {
		        tooltip : {
		            trigger: 'axis',
		            axisPointer : {            // 坐标轴指示器，坐标轴触发有效
		                type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
		            }
		        }, 
		        legend: {
		            data:['月度放款额','月度还款额']
		        },
		        grid: {
		            left: '3%',
		            right: '4%',
		            bottom: '3%',
		            containLabel: true
		        },
		        xAxis : [
		            {
		                type : 'category',
		                data : ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月']
		            }
		        ],
		        yAxis : [
		            {
		                type : 'value',
//		                name:'万'
		            }
		        ],
		        series : [
		            {
		                name:'月度放款额',
		                type:'bar',
				       data:[first,second,third,fourth,fifth,sixth,seventh,eighth,ninth,tenth,eleven,twelve]
		            },
		            {
		                name:'月度还款额',
		                type:'bar',
				        data:[firsts,seconds,thirds,fourths,fifths,sixths,sevenths,eighths,ninths,tenths,elevens,twelves]
		            }
		            
		            
		        ]
		    };
		
		    // 使用刚指定的配置项和数据显示图表。
		    myChart.setOption(option);
				
				
			} else if (data.code == 'OVERTIME'){
				var thisUrl = window.location.href;
				if (thisUrl.indexOf('login.html') <= -1) {
					top.window.location.href="login.html";
				}

			} else {
				if (data.msg != '空数据') {
					alert(data.msg)
				}else{
					$('#thiscount').text(0);
				}
			}

		},
		error:function() {
			alert("error");
		}
	});
	
	
}


//填充年份下拉
function addYears(){
	var myDate = new Date();
	year=myDate.getFullYear();
	var year1 = year -1;
	var year2 = year -2;
	var year3 = year -3;
	var year4 = year -4;
	var year5 = year -5;
	var html = "<option value='"+year+"'>"+year+"年</option>";
	html = html + "<option value='"+year1+"'>"+year1+"年</option>";
	html = html + "<option value='"+year2+"'>"+year2+"年</option>";
	html = html + "<option value='"+year3+"'>"+year3+"年</option>";
	html = html + "<option value='"+year4+"'>"+year4+"年</option>";
	html = html + "<option value='"+year5+"'>"+year5+"年</option>";

	$('#year').append(html);
}


function allWateMoney(){
	$.ajax({
        url:  urlcore + "/api/loanOrder/allWateMoney",
        type: "get",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success:function(data){
            if (data.success == true) {
				$('#watetMoney').html(data.data);
				
            } else if (data.code == 'OVERTIME') {
                var thisUrl = window.location.href;
                if (thisUrl.indexOf('login.html') <= -1) {
                    top.window.location.href = "login.html";
                }
            } else {
                alert(data.msg);
            }

        },
        error: function() {
            alert("error");
        }

    });   

}

